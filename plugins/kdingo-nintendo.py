from plugins import Input

import urllib
import re
import os.path
import datetime

import HTMLParser
import htmlentitydefs
class _KDingoParser(HTMLParser.HTMLParser):
	"""
	Parses the links from a page
	"""
	
	
	def __init__(self):
		HTMLParser.HTMLParser.__init__(self)
		self.objects=[]
		self.cur={}
		self.mode=''
		

	def handle_starttag(self,tag,attrs):
		if tag=='td':
			self.start_td(attrs)
		if tag=="p":
			self.start_p(attrs)
		if tag=='div':
			self.start_div(attrs)
		if tag=="img":
			self.start_img(attrs)
		if tag=="a":
			self.start_a(attrs)
		
	def handle_endtag(self,tag):
		if tag=="div":
			self.end_div()
		if tag=="p":
			self.end_p()
		if tag=='td':
			self.end_td()

	def handle_entityref(self,name):
		self.handle_data(htmlentitydefs.entitydefs[name])

	# handle the data	
	def start_td(self,attributes):
		self.cur={}
	def end_td(self):
		if self.cur.has_key('image') and self.cur.has_key('title') and self.cur.has_key('link'):		# has everything
			self.objects.append(self.cur)
		
		
	def start_p(self,attributes):
		for name,value in attributes:
			if name=='class' and 'giTitle' in value:
				self.mode='title'
	def end_p(self):
		self.mode=''
		
	def start_div(self,attributes):
		for name,value in attributes:
			if name=='class':
				if 'date' in value:
					self.mode='date'
	def end_div(self):
		self.mode=''
		
	def start_a(self, attributes):
		for name,value in attributes:
			if name=="href":
				if not self.cur.has_key('link'):
					self.cur['link']=''
				self.cur['link']+=value
	def start_img(self, attributes):
		for name,value in attributes:
			if name=='src':
				if not self.cur.has_key('image'):
					self.cur['image']=''
				self.cur['image']+=value
		
	def handle_data(self,data):
		if self.mode=='title':
			self.cur['title']=data.strip()
		if self.mode=='date':
			temp=re.search('(\d{2}).(\d{2}).(\d{4})',data.strip()).groups()
			self.cur['date']=datetime.datetime(int(temp[2]),int(temp[0]),int(temp[1]))
			
			

class KDingoNintendo(Input):
    plugin=True     # whether this is a complete plugin
    name="kdingo"   # what plugin name this plugin should handle

    # standard metadata that this feed should provide
    metadata={\
        "title":"KDingo's Nintendo Fanart",\
        "link":"http://www.kdingo.net/champ/pics/main.php?g2_itemId=1984",\
        "language":"en-us"\
    }
    
    srcpage="http://www.kdingo.net/champ/pics/" # what page has the most recent VGCats
    imageregex='<img src="(images/.*\.jpg)"' # what regex to use to grab the image out of the image tag
    backregex='<a href="(\?strip_id=.*)"><img src="back.gif" border="0">'   # what regex to use to grab the link out of the back tag
    
    def getItems(self,path):
        """
        Path is the location on the website to generate an RSS from. This plugin doesn't use it
        """
        
        f=urllib.urlopen(self.metadata['link'])
        s=f.read()
        parser=_KDingoParser()
        parser.feed(s)
        f.close()
        
        list=[]
        for item in parser.objects:
        	temp={}
        	temp['description']="<a href=\"%s\"><img src=\"%s\" /></a>"%(os.path.join(self.srcpage,item['link']),os.path.join(self.srcpage,item['image']))
        	#print "Handling link "+item['link']
        	temp['link']=os.path.join(self.srcpage,item['link'])
        	temp['title']=item['title']
        	temp['date']=item['date']
        	list.append(temp)
        return list

