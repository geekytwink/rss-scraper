from plugins import Input

import urllib
import urlparse
import re
import os.path
import datetime

import HTMLParser
import htmlentitydefs
class _BashParser(HTMLParser.HTMLParser):
	"""
	Parses the links from a page
	"""
	
	
	def __init__(self):
		HTMLParser.HTMLParser.__init__(self)
		self.objects=[]
		self.cur={}
		self.mode=''
		

	def handle_starttag(self,tag,attrs):
		if tag=="p":
			self.start_p(attrs)
		if tag=="a":
			self.start_a(attrs)
		if tag=="b":
			self.start_b(attrs)
		if tag=='br':
			self.handle_data('<br />')
		
	def handle_endtag(self,tag):
		if tag=="p":
			self.end_p()
		if tag=="a":
			self.end_a()
		if tag=="b":
			self.end_b()

	def handle_entityref(self,name):
	#	print('Parsing %s'%name)
		if (name=='lt'):
			self.handle_data('&lt;')
		elif (name=='gt'):
			self.handle_data('&gt;')
		else:
			self.handle_data(htmlentitydefs.entitydefs[name])

	# handle the data	
	def start_p(self,attributes):
		for name,value in attributes:
			if name=='class' and 'quote' in value:
				self.mode='quote'
				self.cur={}
			if name=='class' and 'qt' in value:
				self.mode='quote'
	def end_p(self):
		self.mode=''
		if self.cur.has_key('quote') and self.cur.has_key('link'):
			self.objects.append(self.cur)
		
	def start_a(self, attributes):
		for name,value in attributes:
			if name=="href" \
				and self.mode=='quote' \
				and "rox" not in value \
				and "sux" not in value \
				and "sox" not in value:
				if not self.cur.has_key('link'):
					self.cur['link']=''
				self.cur['link']+=value
				self.mode='quotelink'
	def end_a(self):
		if self.mode=='quotelink':
			self.mode='quote'

	def start_b(self,attributes):
		if self.mode=='quotelink':
			self.mode='quotelinktitle'
	def end_b(self):
		if self.mode=='quotelinktitle':
			self.mode='qoutelink'
		
	def handle_data(self,data):
		if self.mode=='quote':
			if not self.cur.has_key('quote'):
				self.cur['quote']=''
			self.cur['quote']+=data
		if self.mode=='quotelinktitle':
			if not self.cur.has_key('title'):
				self.cur['title']=''
			self.cur['title']+=data
			
			

class Bash(Input):
    plugin=True     # whether this is a complete plugin
    name="bash"   # what plugin name this plugin should handle

    # standard metadata that this feed should provide
    metadata={\
        "title":"QDB: Latest 50 Quotes",\
        "link":"http://bash.org/?latest",\
        "language":"en-us"\
    }
    urlbase='http://bash.org/'
    
    def getItems(self,path):
        """
        Path is the location on the website to generate an RSS from. This plugin doesn't use it
        """
        
        f=urllib.urlopen(self.metadata['link'])
        s=f.read()
        parser=_BashParser()
        parser.feed(s)
        f.close()
        
        list=[]
        for item in parser.objects:
        	temp={}
        	temp['description']=item['quote']
        	temp['link']=urlparse.urljoin(self.metadata['link'],item['link'])
        	temp['title']=item['title']
        	list.append(temp)
        return list

