from plugins import Input

import urllib
import re
import os.path
import random
import datetime

class Nifty(Input):
    plugin=True     # whether this is a complete plugin
    name="nifty"   # what plugin name this plugin should handle

    # standard metadata that this feed should provide
    metadata={\
        "title":"Nifty Erotic Stories",\
        "link":"http://www.nifty.org/",\
        "language":"en-us"\
    }

    srcpages=['http://nifty.org/nifty/',
              'http://nifty.guiltygroups.com/nifty/',
              'http://library.gaycafe.com/nifty/']
    dateregex='<tr><td align=right>.{,5}</td><td align=right>([a-zA-Z]{,3}) [ ]?([0-9]{,2}) (.{,5})</td>' # what regex to use to grab the image out of the image tag
    linkregex='<a href="(.+)">(.+)</a>'   # what regex to use to grab the link and title out of a tag

    year2num={'Jan':1,'Feb':2,'Mar':3,'Apr':4,'May':5,'Jun':6,'Jul':7,'Aug':8,'Sep':9,'Oct':10,'Nov':11,'Dec':12}
    
    def getItems(self,path):
        """
        Path is the location on the website to generate an RSS from. This plugin doesn't use it
        Url is the webpage to parse for pictures and a previous link
        Backlinks is the number of links to go backwards through the VGCats history
        """

        self.metadata['title']="Nifty Erotic Stories "+path
        self.metadata['link']=os.path.join(self.srcpages[0],path)
        
        items=[]

        srcpage=random.choice(self.srcpages)        
        page=os.path.dirname(os.path.join(srcpage,path))
        f=urllib.urlopen(page)   # download the webpage
        lines=f.readlines()
        
        for line in lines:      # go through each line of the webpage
            if '<tr><td align=right>' in line:       # if this is a chapter row
                dateinfo=re.search(self.dateregex,line).groups()        # parse the time
                itemmonth=self.year2num[dateinfo[0]]
                itemday=int(dateinfo[1])
                if ':' in dateinfo[2]:
                    curdate=datetime.date(1,1,1).today()
                    if curdate.month<itemmonth:
                        itemyear=curdate.year-1
                    else:
                        itemyear=curdate.year
                else:
                    itemyear=int(dateinfo[2])
                date=datetime.date(itemyear,itemmonth,itemday)

                linkinfo=re.search(self.linkregex,line).groups()
                link=os.path.join(self.srcpages[0],path,linkinfo[0])
                title=linkinfo[1] # remove the mirror from the page

                
                item={"description":"<a href=\"%s\">%s</a>"%(link,title),"link":link,"title":title,"date":date}
                items.append(item)
        return items

