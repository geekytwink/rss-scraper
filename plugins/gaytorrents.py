from plugins import Input

import urllib
import re
import os.path
import sys
import urlparse
import hashlib
import urllib2
import cookielib

class Gaytorrents(Input):
	plugin=True	 # whether this is a complete plugin
	name="gaytorrents"   # what plugin name this plugin should handle

	username='hufman'
	password='foolproof'

	# standard metadata that this feed should provide
	metadata={\
		"title":"Gaytorrents",\
		"link":"http://www.gay-torrents.net/",\
		"language":"en-us",\
		"description":"Gay Torrents"
	}

	cookiefile='/tmp/gaytorrents.cookie'
	
	
	saltregex="<form.*action=\"(.*?)\".*onsubmit=\"hashLoginPassword\(this, '(.*?)'\);\"" # to get the salt from the login page
	linkregex='<a href="(.*?)/torrent/(.*?)">(.*?)</a>'   # what regex to use to grab the link and title out of a tag
	
	# download links
	downloadregex='<form name="torrentform" action="(.*?)" method="post">'
	torrentidregex='<input type="hidden" name="torrentid" value="(.*?)">'
	infohashregex='<input type="hidden" name="infohash" value="(.*?)">'
	downloadpage='http://www.gay-torrents.net/torrent/'
	downloadlink="http://www.gay-torrents.net/index.php?action=torrentdownload"
	
	def __init__(self):
		if hasattr(Input,'__input__'):
			X.__init__(self)
			
		self.cookiejar=cookielib.LWPCookieJar(self.cookiefile)
		if (os.path.exists(self.cookiefile)):
			self.cookiejar.load()
		self.opener=urllib2.build_opener(urllib2.HTTPCookieProcessor(self.cookiejar))
	def login(self,url,salt):
		inner=hashlib.sha1(self.username.lower()+self.password).hexdigest()
		hashedpassword=hashlib.sha1(inner+salt).hexdigest()
		#hashedpassword=''
		
		mode=''

		formdata=urllib.urlencode({'user':self.username,'hash_passwrd':hashedpassword,'passwrd':'','cookielength':60,'cookieneverexp':'true'})
		#print(formdata)
		data=self.opener.open(url,formdata)
		for line in data.readlines():
			#print(line),
			if "<title>Login</title>" in line:		# is a login page
				mode='login'
				#print("Turning on login mode")
			if mode=='login' and 'hashLoginPassword' in line:	# found the salt for logging in
				submitinfo=re.search(self.saltregex,line).groups()
				#print("Found salt "+submitinfo[1]+" to url "+submitinfo[0])
				self.login(submitinfo[0],submitinfo[1])
				
				#for cookie in self.cookiejar:
					#print("Found precookie "+str(cookie))
					
				data=self.opener.open(submitinfo[0],formdata)
				#print(data.read())
				data.close()
				#for cookie in self.cookiejar:
					#print("Found cookie "+str(cookie))
				return
		data.close()
		#for cookie in self.cookiejar:
			#print("Found post cookie "+str(cookie))
		self.cookiejar.save()
	def download(self,torrentid):
	
		f=self.opener.open(self.downloadpage+torrentid)
		for line in f.readlines():
			info=re.search(self.infohashregex,line)
			if info:
				infohash=info.groups()[0]
				break
		f.close()
		formdata=urllib.urlencode({'torrentid':torrentid,'download':'Download as Torrent','infohash':infohash})
		f=self.opener.open(self.downloadlink,formdata)
		
		sys.stdout.write("Content-type: application/x-bittorrent\n")
		sys.stdout.write("Content-Disposition: attachment; filename=%s.torrent\n\n"%torrentid)
		sys.stdout.write(f.read())
		f.close()
		return torrentid+".torrent"
	def getItems(self,path):
		"""
		Path is the location on the website to generate an RSS from. This plugin doesn't use it
		Url is the webpage to parse for pictures and a previous link
		Backlinks is the number of links to go backwards through the VGCats history
		"""

		self.metadata['title']="Gaytorrents "+path
		#print("link "+self.metadata['link'])
		self.metadata['link']=urlparse.urljoin(self.metadata['link'],path)
		#print("link "+self.metadata['link'])
		items=[]
		
		if 'downloadtorrent/' in path:
			self.download(path.split('/')[-1])
			return

		f=self.opener.open(self.metadata['link'])   # download the webpage
		lines=f.readlines()
		
		mode=''
		for line in lines:	  # go through each line of the webpage
			#print(line),
			if "<title>Login</title>" in line:		# is a login page
				mode='login'
				print("Turning on login mode")
			if mode=='login' and 'hashLoginPassword' in line:	# found the salt for logging in
				submitinfo=re.search(self.saltregex,line).groups()
				#print("Found salt "+submitinfo[1]+" to url "+submitinfo[0])
				self.login(submitinfo[0],submitinfo[1])
				self.cookiejar.save()
				
				return self.getItems(path)

			if mode=='' and '<a href="/torrent/' in line:	   # if this is a link to a torrent
				linkinfo=re.search(self.linkregex,line)
				if not linkinfo:
					continue
				linkinfo=linkinfo.groups()
				reallink=urlparse.urljoin(Gaytorrents.metadata['link'],linkinfo[0]+'/torrent/'+linkinfo[1])
				
				link=urlparse.urljoin(self.base,'downloadtorrent/')
				link=urlparse.urljoin(link,linkinfo[1])
				
				title=linkinfo[2] # remove the mirror from the page
				title=title.replace("&nbsp;"," ")
				title=title.strip()
				
				item={"description":"<a href=\"%s\">%s</a>"%(reallink,title),"link":reallink,"title":title,'guid':reallink}
				item['enclosure']={'type':'application/x-bittorrent', 'url':link, 'length':32767}
				item['debug']=self.base
				items.append(item)
		self.cookiejar.save()
		return items

