from plugins import Input

import urllib
import re
import os.path

class VGCats(Input):
    plugin=True     # whether this is a complete plugin
    name="vgcats"   # what plugin name this plugin should handle

    # standard metadata that this feed should provide
    metadata={\
        "title":"VG Cats Comics",\
        "link":"http://www.vgcats.com/",\
        "language":"en-us"\
    }
    
    srcpage="http://www.vgcats.com/comics/" # what page has the most recent VGCats
    imageregex='<img src="(images/.*\.jpg)"' # what regex to use to grab the image out of the image tag
    backregex='<a href="(\?strip_id=.*)"><img src="back.gif" border="0">'   # what regex to use to grab the link out of the back tag
    
    def getItems(self,path,url=None,backlinks=1):
        """
        Path is the location on the website to generate an RSS from. This plugin doesn't use it
        Url is the webpage to parse for pictures and a previous link
        Backlinks is the number of links to go backwards through the VGCats history
        """
        if not url:         # if no url was given
            url=self.srcpage    # start at the most recent comic
        
        f=urllib.urlopen(url)   # download the webpage
        lines=f.readlines()
        backlink=''
        
        for line in lines:      # go through each line of the webpage
            if '<img src="back.gif" border="0">' in line:       # if a back link is here, get the link
                backlink=re.search(self.backregex,line).groups()[0]
            if "<img src=\"images/" in line:                    # if the picture is here, get it
                file=re.search(self.imageregex,line).groups()[0]

        description="<img src=\"%s\" />"%os.path.join(self.srcpage,file)
        item={"description":description,"link":url,"title":file}

        if backlinks>0 and backlink!='':     # if there's some backlinks left, recurse back through them
            list=self.getItems(path,os.path.join(url,backlink),backlinks-1)
            list.append(item)
            return list
        else:
            return [item]

