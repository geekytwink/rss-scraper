from plugins import Output

class RSS(Output):
    plugin=True
    name="rss"
    num2month={1:'Jan',2:'Feb',3:'Mar',4:'Apr',5:'May',6:'Jun',7:'Jul',8:'Aug',9:'Sep',10:'Oct',11:'Nov',12:'Dec'}
    def generateHeader(self):
        return "Content-type: application/rss+xml; charset=utf-8\n\n<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<rss version=\"2.0\">\n<channel>\n"
    def generateMetadata(self,metadata):
        output=''
        for key in metadata.keys():
            output+="<%s>%s</%s>\n"%(key,self.htmlquote(metadata[key]),key)
        return output
    def generateItem(self,item):
        output='<item>\n'
        for key in item.keys():
            if key=="date":
                month=self.num2month[item[key].month]
#                output+="<pubDate>%s</pubDate>\n"%(item[key].strftime("%%d %s %%y"%month));
                output+="<pubDate>%s</pubDate>\n"%(item[key].strftime("%a, %d %b %Y %H:%M:%S GMT"));
            elif key=='enclosure':
                params=' '.join(['%s="%s"'%(param,item[key][param]) for param in item[key].keys()])
                output+="<enclosure %s/>\n"%params
            else:
                output+="<%s>%s</%s>\n"%(key,self.htmlquote(item[key]),key)

        return output+"</item>\n";
    def generateFooter(self):
        return "</channel></rss>"
