## Thanks to http://lucumr.pocoo.org/blogarchive/python-plugin-system

import os.path
import urllib
import shutil
class Input(object):
    metadata={}
    
    def getItems(self,path):
        return []
        
    def getMetadata(self):
        return self.metadata
        
    def __repr__(self):
        return '<Plugin:%s>'%self.__class__.__name__

class Output(object):
    subs=[\
        ('&','&amp;'),\
        ('>','&gt;'),\
        ('<','&lt;')\
        ]
    def htmlquote(self,data):
        temp=data
        for (char,html) in self.subs:
            temp=temp.replace(char,html)
        return temp
    def generateOutput(self,metadata,items):
        output=self.generateHeader()
        output+=self.generateMetadata(metadata)
        for item in items:
            output+=self.generateItem(item)
        output+=self.generateFooter()
        return output
    def generateHeader(self):
        return ""
    def generateMetadata(self,metadata):
        return ""
    def generateItem(self,item):
        return ""
    def generateFooter(self):
        return ""

import os
import os.path
import sys

def loadplugins():
    """
    Find and import all of the plugins found in the plugins subdirectory of the current directory
    """
    pluginpath=os.path.join(os.curdir,"plugins")
    if not pluginpath in sys.path:
        sys.path.insert(0,pluginpath)
    files=os.listdir(pluginpath)

    pluginfiles=[]
    for filename in files:
        if filename.split('.')[-1]!='py':
            continue
        pluginfiles.append(filename.split('.')[0])

    for plugin in pluginfiles:
        __import__(plugin, None, None, [''])

def getplugins(classname):
    """
    Get all of the plugins of a certain class type

    Arguments:
        classname: A superclass

    Returns: A list ofsubclasses of the main class    
    """
    plugins=[]
    for plugin in classname.__subclasses__():
        if hasattr(plugin,'plugin') and plugin.plugin:
            plugins.append(plugin)
        else:
            plugins.extend(initplugins(plugin))
    return plugins
