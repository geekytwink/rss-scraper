#!/usr/bin/python

import os
import sys
import plugins

output="rss"


plugins.loadplugins()

sourceslist=plugins.getplugins(plugins.Input)
sources={}
for source in sourceslist:
	sources[source.name]=source

generator=None
xmlgenerators=plugins.getplugins(plugins.Output)
for xmlgenerator in xmlgenerators:
	if xmlgenerator.name==output:
		generator=xmlgenerator()

module=''
path=''

if "PATH_INFO" in os.environ and len(os.environ["PATH_INFO"])>0:
	cgipath=os.environ["PATH_INFO"]
	if cgipath[0]=='/':
		cgipath=cgipath[1:]
	splits=cgipath.split("/",1)
	module=splits[0]
	if len(splits)>1:
		path=splits[1]
elif "QUERY_STRING" in os.environ and len(os.environ["QUERY_STRING"])>0:
	import cgi
	qs=cgi.parse_qs(os.environ["QUERY_STRING"])
	if 'module' in qs:
		module=qs['module']
	if 'path' in qs:
		path=qs['path']
else:
	if len(sys.argv)>1:
		module=sys.argv[1]
	if len(sys.argv)>2:
	   path=sys.argv[2]

if module in sources:
	plugin=sources[module]()
	if os.environ.has_key('SERVER_NAME') and os.environ.has_key('SCRIPT_NAME'):
		plugin.base="http://%s%s/%s/"%(os.environ['SERVER_NAME'],os.environ['SCRIPT_NAME'], plugin.name)
	else:
		plugin.base=''
	items=plugin.getItems(path)
	if items:
		output=generator.generateOutput(plugin.getMetadata(),items)
		print output
else:
	if os.environ.has_key('SCRIPT_NAME'):
		print "Content-type: text/html\n"
		# print out environment
		print "<!--"
		for key in os.environ.keys():
			print("%s: %s"%(key,os.environ[key]))
		print "-->"
	else:
		print "Available plugins:"
		
	for sourcename in sources:
		source=sources[sourcename]
		name=source.name
		title=source.name
		if source.metadata.has_key('title'):
			title=source.metadata['title']
			
		if os.environ.has_key('SCRIPT_NAME'):
			print '<a href="%s/%s/">%s</a><br />'%(os.environ['SCRIPT_NAME'], name, title)
		else:
			print '<a href="%s/">%s</a>'%(name, title)
